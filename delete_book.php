<?php
session_start();

if (!isset($_SESSION["user"]) || ($_SESSION["user"]["SZEREPKOR"] != "dolgozo" && $_SESSION["user"]["SZEREPKOR"] != "admin")) {
    header("Location: login.php");
    exit();
}

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
    header("Location: index.php");
    exit();
}

$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE");
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$bookId = $_GET['id'];
$sql = "DELETE FROM KONYV WHERE ID = :book_id";
$stmt = oci_parse($conn, $sql);
oci_bind_by_name($stmt, ':book_id', $bookId);

$result = oci_execute($stmt);

if ($result) {
    oci_commit($conn);
    oci_free_statement($stmt);
    oci_close($conn);
    header("Location: index.php");
    exit();
} else {
    oci_rollback($conn);
    oci_free_statement($stmt);
    oci_close($conn);
    echo "Hibás törlés.";
    exit();
}
?>