<?php
session_start();
if(!isset($_SESSION["user"])){
    header("Location: login.php");
}
if (($_SESSION["user"]["SZEREPKOR"] !== "dolgozo" && $_SESSION["user"]["SZEREPKOR"] !== "admin")) {
    header("Location: index.php");
    exit();
}

if (!isset($_GET['id'])) {
    echo "Book ID not provided!";
    exit();
}
$sikeres = false;
$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$book_id = $_GET['id'];
$sql = "SELECT * FROM KONYV WHERE ID = :book_id";
$stmt = oci_parse($conn, $sql);
oci_bind_by_name($stmt, ':book_id', $book_id);
oci_execute($stmt);
$book = oci_fetch_assoc($stmt);

if (!$book) {
    echo "Book not found!";
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(!empty($_FILES["image"]["name"])) {
        $image = $_FILES["image"];
        $imageName = $image["name"];
        $imageTmpName = $image["tmp_name"];
        $imagePath = '/php/img/' . $imageName;

        move_uploaded_file($imageTmpName, $_SERVER['DOCUMENT_ROOT'] . $imagePath); //document_root = projekt gyokermappaja
    } else {
        $imagePath = $book['KEP'];
    }

    $update_title_sql = "UPDATE KONYV SET CIM = :new_title, KIADAS = :new_kiadas, MUFAJ = :new_mufaj, OLDALSZAM = :new_oldalszam, KOTES = :new_kotes, 
                 MERET = :new_meret, AR = :new_ar, KIADO = :new_kiado, KEP = :new_kep WHERE ID = :book_id";
    $update_title_stmt = oci_parse($conn, $update_title_sql);

    oci_bind_by_name($update_title_stmt, ':new_title', $_POST['title']);
    oci_bind_by_name($update_title_stmt, ':new_kiadas', $_POST['year']);
    oci_bind_by_name($update_title_stmt, ':new_mufaj', $_POST['genre']);
    oci_bind_by_name($update_title_stmt, ':new_oldalszam', $_POST['pages']);
    oci_bind_by_name($update_title_stmt, ':new_kotes', $_POST['binding']);
    oci_bind_by_name($update_title_stmt, ':new_meret', $_POST['size']);
    oci_bind_by_name($update_title_stmt, ':new_ar', $_POST['price']);
    oci_bind_by_name($update_title_stmt, ':new_kiado', $_POST['publisher']);
    oci_bind_by_name($update_title_stmt, ':new_kep', $imagePath);
    oci_bind_by_name($update_title_stmt, ':book_id', $book_id);

    $result = oci_execute($update_title_stmt);

    if ($result) {
        oci_commit($conn);
        $sikeres = "Sikeres frissítés!";
    }else
    {
        $sikeres = "Sikertelen frissítés!";
    }

    oci_free_statement($update_title_stmt);
}

oci_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/update.css">
    <title>Bookstore - Módosítás</title>
</head>
<body>
<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" >Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>
</nav>
<div class="container">
    <div class="container">
        <form method="post" enctype="multipart/form-data" class="book-form">
            <div class="form-group">
                <label for="title">Cím:</label>
                <input type="text" id="title" name="title" value="<?php echo $book['CIM']; ?>" required>
            </div>

            <div class="form-group">
                <label for="publisher">Kiadó:</label>
                <input type="text" id="publisher" name="publisher" value="<?php echo $book['KIADO']; ?>" required>
            </div>

            <div class="form-group">
                <label for="year">Kiadás éve:</label>
                <input type="number" id="year" name="year" value="<?php echo $book['KIADAS']; ?>" required>
            </div>

            <div class="form-group">
                <label for="genre">Műfaj:</label>
                <input type="text" id="genre" name="genre" value="<?php echo $book['MUFAJ']; ?>" required>
            </div>

            <div class="form-group">
                <label for="pages">Oldal szám:</label>
                <input type="number" id="pages" name="pages" value="<?php echo $book['OLDALSZAM']; ?>" required>
            </div>

            <div class="form-group">
                <label for="binding">Kötés típus:</label>
                <input type="text" id="binding" name="binding" value="<?php echo $book['KOTES']; ?>" required>
            </div>

            <div class="form-group">
                <label for="size">Méret:</label>
                <input type="text" id="size" name="size" value="<?php echo $book['MERET']; ?>" required>
            </div>

            <div class="form-group">
                <label for="price">Ár:</label>
                <input type="number" id="price" name="price" value="<?php echo $book['AR']; ?>" required>
            </div>

            <div class="form-group">
                <label for="image">Borítókép (PNG or JPG):</label>
                <input type="file" id="image" name="image" accept="image/png, image/jpeg">
            </div>

            <input type="submit" value="Mentés">
            <?php if(isset($sikeres)) echo $sikeres?>
        </form>
</div>
</body>
</html>