<?php
session_start();
if(!isset($_SESSION["user"])) {
    header("Location: login.php");
    exit;
}
if ($_SESSION["user"]["SZEREPKOR"] != "admin") {
    header("Location: index.php");
    exit;
}

$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE");
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $role = $_POST["role"];

    $sql = "UPDATE FELHASZNALO SET SZEREPKOR = :role WHERE EMAIL = :email";
    $stmt = oci_parse($conn, $sql);

    oci_bind_by_name($stmt, ':role', $role);
    oci_bind_by_name($stmt, ':email', $email);

    $result = oci_execute($stmt, OCI_DEFAULT);
    $sikeres = false;
    if (!$result) {
        oci_rollback($conn);
    } else {
        oci_commit($conn);
        $sikeres = true;
    }

    oci_free_statement($stmt);
    oci_close($conn);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/admin.css">
    <title>Szerepkör változtatás</title>
</head>
<body>
<header>
    <h2>Szerepkör változtatás</h2>
</header>
<nav>
    <a href="index.php">Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php">Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>
</nav>
<form action="admin.php" method="post">
    <label for="email">Email cím:</label>
    <input type="email" id="email" name="email" required>

    <label for="role">Szerepkör:</label>
    <select id="role" name="role" required>
        <option value="vasarlo">Vásárló</option>
        <option value="dolgozo">Dolgozó</option>
        <option value="admin">Admin</option>
    </select>

    <button type="submit">Frissítés</button>
</form>

<?php if(isset($sikeres) && $sikeres): ?>
    <p>Sikeres frissítés</p>
<?php elseif(isset($sikeres) && !$sikeres): ?>
    <p>Sikertelen frissítés</p>
<?php endif; ?>
<h3 style="text-align: center; font-size: 20px">Statisztikák</h3>
<h3 style="text-align: center"><a href="bevetelperkonyv.php">Bevétel/könyv</a></h3>
<h3 style="text-align: center"><a href="atlagbevetelperkateg.php">Átlag bevétel műfajonkénty</a></h3>

</body>
</html>
