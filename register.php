<?php
session_start();
$errors = [];
$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

if (isset($_POST["register"])) {
    // Validation
    if (!isset($_POST["email"]) || trim($_POST["email"]) === "") {
        $errors[] = "Az email cím megadása kötelező!";
    }

    if (!isset($_POST["password"]) || trim($_POST["password"]) === "") {
        $errors[] = "A jelszó megadása kötelező!";
    }

    if ($_POST["password"] !== $_POST["password-check"]) {
        $errors[] = "A két megadott jelszó nem egyezik meg!";
    }

    if (!isset($_POST["address"]) || trim($_POST["address"]) === "") {
        $errors[] = "A lakcím megadása kötelező!";
    }

    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $errors[] = "Rossz email cím formátum!";
    }

    $email = $_POST["email"];
    $stmt = oci_parse($conn, "SELECT COUNT(*) AS count FROM FELHASZNALO WHERE EMAIL = :email");
    oci_bind_by_name($stmt, ":email", $email);
    $result= oci_execute($stmt);
    $row = oci_fetch_assoc($stmt);
    if ($row['COUNT'] > 0) {
        $errors[] = "Foglalt email cím!";
    }


    $password = $_POST["password"];
    if (strlen($password) < 6) {
        $errors[] = "A jelszónak legalább 6 karakter hosszúnak kell lennie!";
    }

    if (empty($errors)) {
        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $address = $_POST["address"];
        $stmt = oci_parse($conn, "INSERT INTO FELHASZNALO (EMAIL, JELSZO, TORZSVASARLO, CIM, SZEREPKOR) VALUES (:email, :password, 0, :address, 'vasarlo')");
        oci_bind_by_name($stmt, ":email", $email);
        oci_bind_by_name($stmt, ":password", $hashed_password);
        oci_bind_by_name($stmt, ":address", $address);
        oci_execute($stmt);

        oci_free_statement($stmt); //memoriaban elengedi elvileg amit lekerdeztunk az adatbazisabn
        oci_close($conn);

        header("Location: login.php");
        exit;
    }
}

?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/login.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Regisztráció</title>
</head>
<body>
<header>
    <h1 class="center">Regisztráció</h1>
</header>
<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="#">Keresés</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>


</nav>

<main>
    <p class="warning center" style="font-size: 15px;">
        <strong>Figyelem!</strong> A <span class="required">*</span>-al jelölt mezők kitöltése kötelező!
    </p>

    <div class="content">
        <div class="form-container">
            <?php if (!empty($errors)) : ?>
                <div class="error">
                    <?php foreach ($errors as $error) : ?>
                        <p><?php echo $error; ?></p>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <form action="register.php" method="post" autocomplete="off" enctype="multipart/form-data">
                <label for="email-label">E-mail cím<span class="required">*</span>:</label>
                <input type="email" name="email" id="email-label" required>

                <label for="password-label">Jelszó (Legalább 6 karakter)<span class="required">*</span>:</label>
                <input type="password" name="password" id="password-label" required>

                <label for="password-check-label">Jelszó még egyszer<span class="required">*</span>:</label>
                <input type="password" name="password-check" id="password-check-label" required>

                <label for="address">Lakcím<span class="required">*</span>:</label>
                <input type="text" name="address" id="address" placeholder="6422,Tompa, Majom utca, 92/B, 2. emelet, 2 ajtó" required>

                <input type="reset" name="reset" value="Mezők törlése">
                <input type="submit" name="register" value="Regisztráció">
            </form>
            Van fiókod? <a href="login.php">Bejelentkezés</a>

        </div>
    </div>
    <!--<p style="text-align: center"><?php // if(isset($result)){if($result){ echo "sikeres regisztráció";} else echo "sikeres regisztráció";}?></p>
-->
</main>

</body>
</html>
