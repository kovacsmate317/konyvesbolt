<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mainpage.css">
    <link rel="stylesheet" href="css/kosar.css">
    <title>Könyvfej</title>
</head>
<body>
<header class="color-transition">
    <h1>Könyvfej</h1>
</header>
<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="kosar.php" id="active">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    session_start();
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" >Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>

</nav>
<div class="container">
    <?php
    if (empty($_SESSION['cart'])) {
        echo '<p>Üres a kosarad.</p>';
    } else {
        $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE");

        if (!$conn) {
            $e = oci_error();
            trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        }

        function updateCartItem($bookId, $quantity) {
            $_SESSION['cart'][$bookId] = $quantity;
        }

        function removeFromCart($bookId) {
            unset($_SESSION['cart'][$bookId]);
        }

        function clearCart() {
            unset($_SESSION['cart']);
        }

        $totalPrice = 0;
        foreach ($_SESSION['cart'] as $bookId => $quantity) {
            $sql = "SELECT * FROM KONYV WHERE ID = :id";
            $stmt = oci_parse($conn, $sql);
            oci_bind_by_name($stmt, ':id', $bookId);
            oci_execute($stmt);

            $book = oci_fetch_assoc($stmt);
            echo '<div class="book">';
            echo '<p>' . $book['CIM'] . '</p>';
            echo '<p>Mennyiség: ' . $quantity . '</p>';
            echo '<p>Ár: ' . $book['AR'] * $quantity . ' Ft</p>';
            echo '<form method="post">';
            echo '<input type="number" name="quantity" min="1" value="' . $quantity . '">';
            echo '<button type="submit" name="update" value="' . $bookId . '">Mennyiség módosítása</button>';
            echo '<button type="submit" name="remove" value="' . $bookId . '">Törlés</button>';
            echo '</form>';
            echo '</div>';

            $totalPrice += $book['AR'] * $quantity;
        }

        echo '<p class="total-price">Teljes összeg: ' . $totalPrice . ' Ft</p>';

        if (isset($_POST['update'])) {
            $bookId = $_POST['update'];
            $quantity = $_POST['quantity'];
            updateCartItem($bookId, $quantity);
            header('Location: kosar.php');
            exit();
        }

        if (isset($_POST['remove'])) {
            $bookId = $_POST['remove'];
            removeFromCart($bookId);
            header('Location: kosar.php');
            exit();
        }



        oci_close($conn);
    }
    ?>
    <form method="post" action="rendeles.php">
        <?php
        if(!empty($_SESSION['cart']) && isset($_SESSION["user"])) {
            echo '<button type="submit" name="order">Megrendelés</button>';
        }elseif (!isset($_SESSION["user"])) {
            echo 'Bekell jelentkezni rendeléshez';
        }
        ?>
        <!--<button type="submit" name="clear" >Kosár ürítése</button>-->

    </form>
</div>

</body>
</html>
