<?php
session_start();

if(isset($_POST['order']) && !empty($_SESSION['cart'])) {
    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

    if (!$conn) {
        $e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    $email = $_SESSION["user"]["EMAIL"];
    $sql_order = "INSERT INTO RENDELES (EMAIL, DATUM,LAKCIM) VALUES (:email, SYSDATE, :lakcim) RETURNING ID INTO :order_id";
    $stmt_order = oci_parse($conn, $sql_order);
    oci_bind_by_name($stmt_order, ':email', $email);
    oci_bind_by_name($stmt_order, ':order_id', $order_id, 10);
    oci_bind_by_name($stmt_order, ':lakcim', $_SESSION["user"]["CIM"]);
    $siker = oci_execute($stmt_order);

    foreach ($_SESSION['cart'] as $bookId => $quantity) {
        $sql_item = "INSERT INTO RENDELES_TETEL (RENDELES_ID, KONYV_ID, MENNYISEG, AR) VALUES (:order_id, :book_id, :quantity, (SELECT AR FROM KONYV WHERE ID = :book_id))";
        $stmt_item = oci_parse($conn, $sql_item);
        oci_bind_by_name($stmt_item, ':order_id', $order_id);
        oci_bind_by_name($stmt_item, ':book_id', $bookId);
        oci_bind_by_name($stmt_item, ':quantity', $quantity);
        oci_execute($stmt_item);
    }

    $totalPrice = 0;
    foreach ($_SESSION['cart'] as $bookId => $quantity) {
        $sql_price = "SELECT AR FROM KONYV WHERE ID = :book_id";
        $stmt_price = oci_parse($conn, $sql_price);
        oci_bind_by_name($stmt_price, ':book_id', $bookId);
        oci_execute($stmt_price);
        $book_price = oci_fetch_assoc($stmt_price);
        $totalPrice += $book_price['AR'] * $quantity;
    }

    oci_commit($conn);

    unset($_SESSION['cart']);

    oci_close($conn);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mainpage.css">
    <link rel="stylesheet" href="css/kosar.css">
    <title>Könyvfej</title>
</head>
<body>
<header class="color-transition">
    <h1>Könyvfej</h1>
</header>
<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="kosar.php" id="active">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php

    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" >Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>

</nav>
<div class="container">
    <?php
    if (isset($siker)) {
        if ($siker) {
            echo '<p>Sikeres rendelés! </p>';
        } else {
            echo '<p>Hibás rendelés!</p>';
        }
    }
    ?>
</div>
</body>
</html>
