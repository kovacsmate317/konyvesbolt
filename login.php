<?php

session_start();
$errors = [];
$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

if (isset($_POST["login"])) {
    if (!isset($_POST["email"]) || trim($_POST["email"]) === ""
        || !isset($_POST["password"]) || trim($_POST["password"]) === "") {
        $errors[] = "Adjon meg minden adatot!";
    } else {
        $email = $_POST["email"];
        $password = $_POST["password"];
        $uzenet = "Sikertelen belépés, hibás adatok!";

        // SQL query
        $sql = "SELECT * FROM FELHASZNALO WHERE EMAIL = :email";
        $stmt = oci_parse($conn, $sql);
        oci_bind_by_name($stmt, ":email", $email);
        oci_execute($stmt);

        // Fetch results
        $user = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS);
        if ($user) {
            /*
            echo "User found in the database: <pre>" . print_r($user, true) . "</pre>"; // Debug
            echo "Provided password: $password"; // Debug
            echo "Hashed password from database: " . $user["JELSZO"]; // Debug
            */
            if (password_verify($password, $user["JELSZO"])) {
                echo "Password verification successful."; // Debugging
                $_SESSION["user"] = $user;
                header("Location: index.php");
                exit;
            } else {
                $errors[] = "Sikertelen belépés, hibás adatok!";
            }
        } else {
            $errors[] = "Felhasználó nem található!";
        }
    }
}

?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bejelentkezés</title>
</head>
<body>
<header>
    <h1 class="center">Bejelentkezés</h1>
</header>
<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="#">Keresés</a>
    <a href="kosar.php">Kosár</a>

    <?php
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php" id="active">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>


</nav>
<main>
    <div>
        <div class="form-container">
            <?php if (!empty($errors)) : ?>
                <div class="error">
                    <?php foreach ($errors as $error) : ?>
                        <p><?php echo $error; ?></p>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <form action="login.php" method="post" autocomplete="on">
                <label for="email-label">E-mail cím:</label>
                <input type="text" name="email" id="email-label" required class="login-outline-off">

                <label for="password-label">Jelszó:</label>
                <input type="password" name="password" id="password-label" required class="login-outline-off">

                <input type="submit" name="login" value="Bejelentkezés">
                Még nem regisztráltál? <a href="register.php">Regisztráció</a>
            </form>
        </div>
    </div>
</main>
</body>
</html>
