<?php
session_start();
if (!isset($_SESSION["user"])) {
    header("Location: login.php");
}
if ($_SESSION["user"]["SZEREPKOR"] != "dolgozo" && $_SESSION["user"]["SZEREPKOR"] != "admin") {
    header("Location: login.php");
    exit;
}
$sikeres = false;
$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Gather form data
    $title = $_POST["title"];
    $quantity = $_POST["quantity"];
    $store_name = $_POST["store_name"];

    $sql_check_store = "SELECT ID FROM BOLT WHERE CIM = :store_name";
    $stmt_check_store = oci_parse($conn, $sql_check_store);
    oci_bind_by_name($stmt_check_store, ':store_name', $store_name);
    oci_execute($stmt_check_store);
    $row_check_store = oci_fetch_assoc($stmt_check_store);
    if (!$row_check_store) {
        $message = "Nincs ilyen bolt.";
    } else {
        $store_id = $row_check_store['ID'];

        $sql_check_book = "SELECT ID FROM KONYV WHERE CIM = :title";
        $stmt_check_book = oci_parse($conn, $sql_check_book);
        oci_bind_by_name($stmt_check_book, ':title', $title);
        oci_execute($stmt_check_book);
        $row_check_book = oci_fetch_assoc($stmt_check_book);
        if (!$row_check_book) {
            $message = "Nincs ilyen könyv.";
        } else {
            $book_id = $row_check_book['ID'];
            $sql_check_raktaron = "SELECT COUNT(*) AS count FROM RAKTARON WHERE BOLT_ID = :store_id AND KONYV_ID = :book_id";
            $stmt_check_raktaron = oci_parse($conn, $sql_check_raktaron);
            oci_bind_by_name($stmt_check_raktaron, ':store_id', $store_id);
            oci_bind_by_name($stmt_check_raktaron, ':book_id', $book_id);
            oci_execute($stmt_check_raktaron);
            $row_check_raktaron = oci_fetch_assoc($stmt_check_raktaron);
            if ($row_check_raktaron['COUNT'] > 0) {
                // ha mar van ilyen cska update
                $sql = "UPDATE RAKTARON SET DB = DB + :quantity WHERE BOLT_ID = :store_id AND KONYV_ID = :book_id";
            } else {
                // ha nincs ilyen
                $sql = "INSERT INTO RAKTARON (BOLT_ID, KONYV_ID, DB) VALUES (:store_id, :book_id, :quantity)";
            }
            $stmt = oci_parse($conn, $sql);
            oci_bind_by_name($stmt, ':store_id', $store_id);
            oci_bind_by_name($stmt, ':book_id', $book_id);
            oci_bind_by_name($stmt, ':quantity', $quantity);
            $result = oci_execute($stmt, OCI_DEFAULT);
            if (!$result) {
                oci_rollback($conn);
            } else {
                oci_commit($conn);
                $sikeres = true;
                $message = "Sikeres felvitel!";
            }
            oci_free_statement($stmt);
            oci_free_statement($stmt_check_raktaron);
        }
        oci_free_statement($stmt_check_book);
    }
    oci_free_statement($stmt_check_store);
    oci_close($conn);
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/konyvadd.css">
    <link rel="stylesheet" href="css/mainpage.css">
    <title>Bookstore</title>
</head>
<body>

<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" id="active">Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php">Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>

</nav>
<div class="container">
    <form action="bolt.php" method="post" style="margin-top: 10px">
        <label for="title">Könyv címe:</label>
        <input type="text" id="title" name="title" autocomplete="on" required>

        <label for="quantity">Mennyiség:</label>
        <input type="number" id="quantity" name="quantity" required>

        <label for="store_name">Bolt neve:</label>
        <input type="text" id="store_name" name="store_name" autocomplete="on" required>

        <input type="submit" value="Hozzáadás">
        <?php if(isset($message)) echo $message; ?>
    </form>
</div>
</body>
</html>
