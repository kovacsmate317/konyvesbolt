<?php
session_start();
if (!isset($_SESSION["user"])) {
    header("Location: login.php");
    exit;
}
if ($_SESSION["user"]["SZEREPKOR"] != "admin") {
    header("Location: index.php");
    exit;
}

$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$sql = "SELECT k.CIM, SUM(rt.AR) AS TOTAL_REVENUE
        FROM RENDELES_TETEL rt
        INNER JOIN KONYV k ON rt.KONYV_ID = k.ID
        GROUP BY k.CIM";
$stmt = oci_parse($conn, $sql);
oci_execute($stmt);
?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/admin.css">
        <link rel="stylesheet" href="css/bevetel.css">
        <title>Total Revenue per Book Title</title>
    </head>
    <body>
    <header>
        <h1>Könyvfej</h1>
    </header>
    <nav>
        <a href="index.php">Kezdőlap</a>
        <a href="kosar.php">Kosár</a>
        <a href="topsell.php">Top 5</a>
        <?php
        if (!isset($_SESSION["user"])) {
            echo '<a href="login.php">Bejelentkezés</a>';
        }
        if (isset($_SESSION["user"])) {
            if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
                echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
            }
            echo '<a href="profile.php">Profilom</a>';
            if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
                echo '<a href="admin.php">Admin</a>';
            }
            echo '<a href="logout.php">Kijelentkezés</a>';
        }
        ?>
    </nav>

    <table>
        <tr>
            <th>Cím</th>
            <th>Teljes bevétel</th>
        </tr>
        <?php
        while ($row = oci_fetch_assoc($stmt)) {
            echo "<tr>";
            echo "<td>" . $row['CIM'] . "</td>";
            echo "<td>" . $row['TOTAL_REVENUE'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>

    </body>
    </html>

<?php
oci_free_statement($stmt);
oci_close($conn);
?>