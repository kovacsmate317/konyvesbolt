--------------------------------------------------------
--  File created - h�tf�-m�jus-06-2024   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Function GET_MOST_ORDERED_GENRE
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "BAMBI"."GET_MOST_ORDERED_GENRE" (user_email VARCHAR2) RETURN VARCHAR2 AS
  genre_name VARCHAR2(100);
BEGIN
  SELECT mufaj
  INTO genre_name
  FROM (
    SELECT k.mufaj, COUNT(*) AS genre_count
    FROM rendeles r
    JOIN rendeles_tetel rt ON r.id = rt.rendeles_id
    JOIN konyv k ON rt.konyv_id = k.id
    WHERE r.email = user_email
    GROUP BY k.mufaj
    ORDER BY COUNT(*) DESC
  )
  WHERE ROWNUM = 1;

  RETURN genre_name;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN NULL;
END get_most_ordered_genre;

/
--------------------------------------------------------
--  DDL for Function GET_ORDER_COUNT
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "BAMBI"."GET_ORDER_COUNT" (p_email IN VARCHAR2) RETURN NUMBER IS
    v_order_count NUMBER;
BEGIN
    SELECT COUNT(*) INTO v_order_count FROM RENDELES WHERE EMAIL = p_email;

    RETURN v_order_count;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 0;
END;

/
