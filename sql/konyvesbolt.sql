--------------------------------------------------------
--  File created - h�tf�-m�jus-06-2024   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table BOLT
--------------------------------------------------------

  CREATE TABLE "BAMBI"."BOLT" 
   (	"ID" NUMBER(*,0), 
	"CIM" VARCHAR2(60 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table FELHASZNALO
--------------------------------------------------------

  CREATE TABLE "BAMBI"."FELHASZNALO" 
   (	"EMAIL" VARCHAR2(40 BYTE), 
	"JELSZO" VARCHAR2(256 BYTE), 
	"TORZSVASARLO" NUMBER(1,0), 
	"CIM" VARCHAR2(60 BYTE), 
	"SZEREPKOR" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

   COMMENT ON COLUMN "BAMBI"."FELHASZNALO"."SZEREPKOR" IS 'dolgozo/vasarlo/admin';
--------------------------------------------------------
--  DDL for Table KONYV
--------------------------------------------------------

  CREATE TABLE "BAMBI"."KONYV" 
   (	"ID" NUMBER(*,0), 
	"CIM" VARCHAR2(100 BYTE), 
	"KIADAS" NUMBER(*,0), 
	"MUFAJ" VARCHAR2(50 BYTE), 
	"OLDALSZAM" NUMBER(*,0), 
	"KOTES" VARCHAR2(50 BYTE), 
	"MERET" VARCHAR2(50 BYTE), 
	"AR" NUMBER(*,0), 
	"KIADO" VARCHAR2(70 BYTE), 
	"KEP" VARCHAR2(250 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RAKTARON
--------------------------------------------------------

  CREATE TABLE "BAMBI"."RAKTARON" 
   (	"BOLT_ID" NUMBER(*,0), 
	"KONYV_ID" NUMBER(*,0), 
	"DB" NUMBER(*,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RENDELES
--------------------------------------------------------

  CREATE TABLE "BAMBI"."RENDELES" 
   (	"ID" NUMBER, 
	"EMAIL" VARCHAR2(30 BYTE), 
	"DATUM" DATE, 
	"LAKCIM" VARCHAR2(60 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table RENDELES_TETEL
--------------------------------------------------------

  CREATE TABLE "BAMBI"."RENDELES_TETEL" 
   (	"ID" NUMBER, 
	"RENDELES_ID" NUMBER, 
	"KONYV_ID" NUMBER, 
	"MENNYISEG" NUMBER, 
	"AR" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table SZERZO
--------------------------------------------------------

  CREATE TABLE "BAMBI"."SZERZO" 
   (	"KONYV_ID" NUMBER(*,0), 
	"FOSZERZO" VARCHAR2(50 BYTE), 
	"ALSZERZO" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into BAMBI.BOLT
SET DEFINE OFF;
Insert into BAMBI.BOLT (ID,CIM) values ('1','K�nyvfej');
REM INSERTING into BAMBI.FELHASZNALO
SET DEFINE OFF;
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('kovascmate.16@gmail.com','$2y$10$.RImJVj4JHlJAdLDqPiYeemaiLsZnWFJvoaVd47GLsIYDxICfvfqu','1','gaty','admin');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('gatya@gatya.com','$2y$10$dJu9KX6xg7r/ZgkKvPiG0uIp7PmsaCWXm3WPBtH1NJhyV.w05QKV2','0','matya','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('teglas@teko.hu','$2y$10$qJz2gDvoKf0AqNHd/F7Pj.b361BXmdhssqmzFlXiTAlGA7w3FzXM.','0','6400, Hirnok utca','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('fodor@bibo.hu','$2y$10$wc.3sA0tna0daZNOhmmOQuPeuleMhw4822XuV63ul0gMN/JgBK1RC','0','6400, Tanya','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('lecso@szilady.hu','$2y$10$jMowB3xzdFMBK5u57TqaN.7bojPQ0rRdQWqoO2Tyfrz.uuoJqogLa','0','6400, Kiskunhalas, temeto','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('vasarlo@konyv.com','$2y$10$v9lmEGCxF7OafdnZFFoyPuI.WGzC3gIdEre/x8NzASWW/oBUyH4M2','0','Windows, desktop 2','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('das@das.das','$2y$10$tO3EI5zzGquRrd4tDcVpJ.WvGXXQcQ/LCL6syBOzqA1d1.4.U7YTO','0','dass utca 1','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('k@k.k','$2y$10$F6nGUSCf9ZSfesw612LTT.pgqB2YSK3Rw81/Dz3geahb7slNqdseS','0','k','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('mm@mm.com','$2y$10$Kxw1hpE1aLNzhG/H4ZOtwezQoaFk7SzC1q.MDtvo9Z6bjkLKv4sn2','0','m','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('g@g.hu','$2y$10$ewfvsyzLBczml13BXbBLFOYCugt9ucpOU1xYBw6xhbBr0qAdrf6Pa','0','g','dolgozo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('l@l.l','$2y$10$u.vMGaFkaYaboGiANn3YaOYQ0bB4mxyE.6q5aBlNY0W9I3nZdIYBu','0','l','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('noeko@freemail.hu','$2y$10$RcgO0BqFHdfEkA4zkKp5TuAv89iANKi5eiMU9yhTHgW8BPksIiz.i','0','Tompa...','dolgozo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('c@c.c','$2y$10$DgmzjkLqZ2GXppDgq1Eupe2MLFGvnDubT9ejj5.oRRseRNsWR7uvu','0','c','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('b@b.b','$2y$10$1Asnp5f/VYznGWF2ucNP6e6rShrzgG1oeWHzNby5XX8izncBe/2Qq','0','b','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('kata@gmail.com','$2y$10$PAeAgnFT7LnZ4KPnY.xyn.GrmHrro9A.lWvo/abLclU8iutR3yBj.','0','Otthon','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('f@f.com','$2y$10$jslkvT7ioVjJt.FZyvYxjO/lSuv5nwkAS1adXC3vzUgkiJsLFFvVe','0','f','dolgozo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('baba@puszi.com','$2y$10$kfv2EB056XRa7z/T1NPyPeImHPsmB6IwP1vRpeUn0pBwT.K1gfCPy','0','6726 Szeged Korondi utca 4/b','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('dolgozo1@dolgozo.com','$2y$10$1bUaITv7HiIphjLYNE41Feoa.Sa8fxofQEaijEvf.YlUcGfGSJwNq','0','6000,lakcim utca, 1','admin');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('egyedul@csinalom.hu','$2y$10$I6efs611xWzLysSR9pGUNuwZ2hkHP5Su7pNOKYvsU19X0izlfYVQC','0','xx','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('egy@ketto.harom','$2y$10$R8.SZYU/qywo37ZAMQVMU.ckCd0ljGUgwCQTgx7.lClj90lPwQ3fu','0','otthon','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('kovacsmate@gmail.com','$2y$10$lC2wXd70xLsk1wwirsKHMOXzRWkcgUi8p9WBdqqOgfzC11KhFROG.','0','6422, Tompa, x utca, 65','admin');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('a@a.com','$2y$10$Id77eYP4.jEkoCOa9qyyO.PQ5u3RlpeysnBhTZLb0tg9Z9/FVPS36','0','aa,aa','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('v@v.v','$2y$10$oKt8SAHpIQPutZwlRopkTekCCLx5Si20YRU3xUB6ERTm6Sh7hafD2','0','v','vasarlo');
Insert into BAMBI.FELHASZNALO (EMAIL,JELSZO,TORZSVASARLO,CIM,SZEREPKOR) values ('kovacs@freemail.hu','$2y$10$Z1FG3ANpBPVcNxtp9E1hG..eoPyQ4/fkfRXABcTHo9r/pbCiBAx1m','0','kispest','dolgozo');
REM INSERTING into BAMBI.KONYV
SET DEFINE OFF;
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('8','Berserk Deluxe Volume 2','2019','S�t�t fantasy','692','Kem�ny k�t�s','193 x 260 x 50','19334','Kentaro Miura','/php/img/20805371_berserk-deluxe-volume-2.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('9','Dune','2005','Sci-fi','535','Puha k�t�s','193 x 108 x 60','4574','Penguin Books','/php/img/04345413o.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('10','Vinland Saga Deluxe 1','2023','Akci�','688','Kem�ny k�t�s','172 x 253','21157','Kodansha America, Inc','/php/img/vinn delux 2.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('11','The 48 Laws of Power','2000','Filoz�fia','496','Puha k�t�s','167 x 236 x 32','9059','Penguin Books','/php/img/01375231_the-48-laws-of-power.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('16','No Longer Human','2019','Sz�pirodalom','176','Puha k�t�s','203 x 131 x 15','5122','New Directions','/php/img/06531169_no-longer-human.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('5','To Kill a Mockingbird','1960','Regeny','324','Puha k�t�s','13x20 cm','2800','J. B. Lippincott & Co.','/php/img/mockingbird.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('15','Murals of Tibet','2018','Filoz�fia','498','Puha k�t�s','700 x 500','5000421','Slovart','/php/img/19299764_murals-of-tibet.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('6','The Shadow of the Wind','1984','Thriller','487','Kem�ny k�t�s',' 15 x 23 cm','4200','Vintage Books','/php/img/shadowofthewind.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('7','1984','2009','Reg�ny','328','Puha k�t�s',' 13 x 20 cm','2800','Penguin Books','/php/img/1984konyv.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('12','Solo Leveling, Vol. 1','2021','K�preg�ny','192','Puha k�t�s','208 x 146 x 22','7863','Little, Brown Book Group','/php/img/33110164o.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('13','Vagabond (VIZBIG Edition), Vol. 6','2014','K�preg�ny','584','Puha k�t�s','218 x 148 x 40','9059','Viz Media','/php/img/02787120_vagabond-vol-6-vizbig-edition.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('3','The Great Gatsby','1925','Novella','218','Kem�ny k�t�s','14x21 cm','3500','Scribner','/php/img/gatsby.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('4','To Kill a Mockingbird','1960','Reg�ny','324','Puha k�t�s','13x20 cm','2800','J. B. Lippincott & Co.','/php/img/mockingbird.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('1','Vinland Saga 1','2013','Akci�','472','Kem�ny k�t�s','147 x 206 x 43','6987','Kodansha America','/php/img/01350055_vinland-saga-1.jpg');
Insert into BAMBI.KONYV (ID,CIM,KIADAS,MUFAJ,OLDALSZAM,KOTES,MERET,AR,KIADO,KEP) values ('2','Vinland Saga 2','2014','Akci�','442','Kem�ny k�t�s','147 x 206 x 43','6963','Kodansha America','/php/img/02087839_vinland-saga-2.jpg');
REM INSERTING into BAMBI.RAKTARON
SET DEFINE OFF;
Insert into BAMBI.RAKTARON (BOLT_ID,KONYV_ID,DB) values ('1','1','11');
REM INSERTING into BAMBI.RENDELES
SET DEFINE OFF;
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('3','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('4','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('5','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('6','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('7','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('9','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('15','dolgozo1@dolgozo.com',to_date('24-M�J.  -06','RR-MON-DD'),'6000,lakcim utca, 1');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('1','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('2','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('11','kovascmate.16@gmail.com',to_date('24-�PR.  -30','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('12','kovascmate.16@gmail.com',to_date('24-�PR.  -30','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('14','kovascmate.16@gmail.com',to_date('24-�PR.  -30','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('8','kovascmate.16@gmail.com',to_date('24-�PR.  -29','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('13','kovascmate.16@gmail.com',to_date('24-�PR.  -30','RR-MON-DD'),'gaty');
Insert into BAMBI.RENDELES (ID,EMAIL,DATUM,LAKCIM) values ('16','dolgozo1@dolgozo.com',to_date('24-M�J.  -06','RR-MON-DD'),'6000,lakcim utca, 1');
REM INSERTING into BAMBI.RENDELES_TETEL
SET DEFINE OFF;
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('1','2','10','1','21157');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('2','2','11','1','9059');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('3','2','16','1','5122');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('18','3','6','1','4200');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('19','4','9','1','4574');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('20','5','8','1','19334');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('21','6','15','1','5000421');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('22','7','9','1','4574');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('24','9','1','1','6987');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('32','15','1','2','6987');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('26','11','9','1','4574');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('27','12','9','3','4574');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('28','12','6','1','4200');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('30','14','9','3','4574');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('31','14','7','1','2800');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('23','8','2','1','6963');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('29','13','6','1','4200');
Insert into BAMBI.RENDELES_TETEL (ID,RENDELES_ID,KONYV_ID,MENNYISEG,AR) values ('33','16','9','2','4574');
REM INSERTING into BAMBI.SZERZO
SET DEFINE OFF;
Insert into BAMBI.SZERZO (KONYV_ID,FOSZERZO,ALSZERZO) values ('2','gatya','matya');
Insert into BAMBI.SZERZO (KONYV_ID,FOSZERZO,ALSZERZO) values ('1','gatya','matya');
--------------------------------------------------------
--  DDL for Index SYS_C009996
--------------------------------------------------------

  CREATE UNIQUE INDEX "BAMBI"."SYS_C009996" ON "BAMBI"."BOLT" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SYS_C009999
--------------------------------------------------------

  CREATE UNIQUE INDEX "BAMBI"."SYS_C009999" ON "BAMBI"."FELHASZNALO" ("EMAIL") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SYS_C009997
--------------------------------------------------------

  CREATE UNIQUE INDEX "BAMBI"."SYS_C009997" ON "BAMBI"."KONYV" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SYS_C0010028
--------------------------------------------------------

  CREATE UNIQUE INDEX "BAMBI"."SYS_C0010028" ON "BAMBI"."RENDELES" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SYS_C0010030
--------------------------------------------------------

  CREATE UNIQUE INDEX "BAMBI"."SYS_C0010030" ON "BAMBI"."RENDELES_TETEL" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Trigger KONYV_ID_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "BAMBI"."KONYV_ID_TRIGGER" 
BEFORE INSERT ON KONYV
FOR EACH ROW
BEGIN
    SELECT NVL(MAX(ID), 0) + 1 INTO :NEW.ID FROM KONYV;
END;

/
ALTER TRIGGER "BAMBI"."KONYV_ID_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger UPDATE_TORZS_INS
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "BAMBI"."UPDATE_TORZS_INS" 
BEFORE INSERT ON RENDELES
FOR EACH ROW
DECLARE
    v_order_count NUMBER;
BEGIN
    SELECT COUNT(*) INTO v_order_count FROM RENDELES WHERE EMAIL = :NEW.EMAIL;
    IF v_order_count > 4 THEN
    
        UPDATE FELHASZNALO SET TORZSVASARLO = 1 WHERE EMAIL = :NEW.EMAIL;
    END IF;
END;
/
ALTER TRIGGER "BAMBI"."UPDATE_TORZS_INS" ENABLE;
--------------------------------------------------------
--  DDL for Trigger RENDELES_ID_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "BAMBI"."RENDELES_ID_TRIGGER" 
BEFORE INSERT ON RENDELES
FOR EACH ROW
BEGIN
    SELECT NVL(MAX(ID), 0) + 1 INTO :NEW.ID FROM RENDELES;
END;
/
ALTER TRIGGER "BAMBI"."RENDELES_ID_TRIGGER" ENABLE;
--------------------------------------------------------
--  DDL for Trigger UPDATE_RAKTARON_QUANTITY
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "BAMBI"."UPDATE_RAKTARON_QUANTITY" 
AFTER INSERT ON RENDELES_TETEL
FOR EACH ROW
BEGIN
    UPDATE RAKTARON
    SET DB = DB - :new.MENNYISEG
    WHERE KONYV_ID = :new.KONYV_ID;
END;

/
ALTER TRIGGER "BAMBI"."UPDATE_RAKTARON_QUANTITY" ENABLE;
--------------------------------------------------------
--  DDL for Trigger RENDELES_TETEL_ID_TRIGGER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "BAMBI"."RENDELES_TETEL_ID_TRIGGER" 
BEFORE INSERT ON RENDELES_TETEL
FOR EACH ROW
BEGIN
    SELECT NVL(MAX(ID), 0) + 1 INTO :NEW.ID FROM RENDELES_TETEL;
END;
/
ALTER TRIGGER "BAMBI"."RENDELES_TETEL_ID_TRIGGER" ENABLE;
--------------------------------------------------------
--  Constraints for Table BOLT
--------------------------------------------------------

  ALTER TABLE "BAMBI"."BOLT" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table FELHASZNALO
--------------------------------------------------------

  ALTER TABLE "BAMBI"."FELHASZNALO" ADD PRIMARY KEY ("EMAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table KONYV
--------------------------------------------------------

  ALTER TABLE "BAMBI"."KONYV" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table RENDELES
--------------------------------------------------------

  ALTER TABLE "BAMBI"."RENDELES" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Constraints for Table RENDELES_TETEL
--------------------------------------------------------

  ALTER TABLE "BAMBI"."RENDELES_TETEL" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RAKTARON
--------------------------------------------------------

  ALTER TABLE "BAMBI"."RAKTARON" ADD FOREIGN KEY ("BOLT_ID")
	  REFERENCES "BAMBI"."BOLT" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "BAMBI"."RAKTARON" ADD FOREIGN KEY ("KONYV_ID")
	  REFERENCES "BAMBI"."KONYV" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RENDELES
--------------------------------------------------------

  ALTER TABLE "BAMBI"."RENDELES" ADD FOREIGN KEY ("EMAIL")
	  REFERENCES "BAMBI"."FELHASZNALO" ("EMAIL") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table RENDELES_TETEL
--------------------------------------------------------

  ALTER TABLE "BAMBI"."RENDELES_TETEL" ADD FOREIGN KEY ("RENDELES_ID")
	  REFERENCES "BAMBI"."RENDELES" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "BAMBI"."RENDELES_TETEL" ADD FOREIGN KEY ("KONYV_ID")
	  REFERENCES "BAMBI"."KONYV" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SZERZO
--------------------------------------------------------

  ALTER TABLE "BAMBI"."SZERZO" ADD FOREIGN KEY ("KONYV_ID")
	  REFERENCES "BAMBI"."KONYV" ("ID") ON DELETE CASCADE ENABLE;
