<?php
session_start();
if (!isset($_SESSION["user"])) {
    header("Location: login.php");
}
?>

<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="img/1984konyv.jpg">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/profile.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profilom</title>
</head>
<body>
<header>
    <h1>könyvfej</h1>
</header>
<nav>
    <a href="index.php">Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if (!isset($_SESSION["user"])) {
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if (isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
        }
        echo '<a href="profile.php" id="active">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php">Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>
</nav>
<main>
    <section>
        <table>
            <tr>
                <th>Email cím</th>
                <td><?php echo $_SESSION["user"]["EMAIL"] ?></td>
            </tr>
            <tr>
                <th>Lakcím</th>
                <td><?php echo $_SESSION["user"]["CIM"] ?></td>
            </tr>
            <tr>
                <th>Rendeléseim</th>
                <td><?php
                    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

                    if (!$conn) {
                        $e = oci_error();
                        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                    }
                    $email = $_SESSION["user"]["EMAIL"];
                    $sql_order_count = "BEGIN :order_count := get_order_count(:email); END;";
                    $stmt_order_count = oci_parse($conn, $sql_order_count);
                    oci_bind_by_name($stmt_order_count, ':email', $email);
                    oci_bind_by_name($stmt_order_count, ':order_count', $order_count, 10);
                    oci_execute($stmt_order_count);
                    echo $order_count;
                    ?>
                </td>
            </tr>
            <tr>
                <th>Kedvenc műfajod:</th>
                <td><?php
                    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

                    if (!$conn) {
                        $e = oci_error();
                        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                    }

                    $user_email = $_SESSION["user"]["EMAIL"];
                    $sql_most_ordered_genre = "BEGIN :most_ordered_genre := get_most_ordered_genre(:user_email); END;";
                    $stmt_most_ordered_genre = oci_parse($conn, $sql_most_ordered_genre);
                    oci_bind_by_name($stmt_most_ordered_genre, ':user_email', $user_email);
                    oci_bind_by_name($stmt_most_ordered_genre, ':most_ordered_genre', $most_ordered_genre, 100);
                    oci_execute($stmt_most_ordered_genre);

                    echo $most_ordered_genre;

                    oci_close($conn);
                    ?>
                </td>
            </tr>
            <tr>
                <th>Rendelések</th>
                <td>
                    <?php
                    $email = $_SESSION["user"]["EMAIL"];
                    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE");

                    if (!$conn) {
                        $e = oci_error();
                        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                    }

                    $sql_orders = "SELECT r.ID AS ORDER_ID, r.DATUM, b.CIM, rt.MENNYISEG, rt.AR 
                                   FROM RENDELES r
                                   JOIN RENDELES_TETEL rt ON r.ID = rt.RENDELES_ID
                                   JOIN KONYV b ON rt.KONYV_ID = b.ID
                                   WHERE r.EMAIL = :email
                                   ORDER BY r.ID";
                    $stmt_orders = oci_parse($conn, $sql_orders);
                    oci_bind_by_name($stmt_orders, ':email', $email);
                    oci_execute($stmt_orders);

                    $current_order_id = null;
                    $order_total = 0;

                    while ($row = oci_fetch_assoc($stmt_orders)) {
                        if ($row['ORDER_ID'] !== $current_order_id) {
                            if ($current_order_id !== null) {
                                echo "<tr><td colspan='4'><strong>Összesen:</strong></td><td style='font-weight: bolder'>$order_total</td></tr>";
                                echo "</table>";
                                $order_total = 0;
                            }
                            $current_order_id = $row['ORDER_ID'];
                            echo "<h3>Rendelés #" . $current_order_id . "</h3>";
                            echo "<table>";
                            echo "<tr style='font-weight: bolder'>";
                            echo "<td>Rendelés dátuma</td>";
                            echo "<td>Könyvek</td>";
                            echo "<td>Mennyiség</td>";
                            echo "<td>Könyvek ára</td>";
                            echo "</tr>";
                        }

                        $book_price = $row['AR'] * $row['MENNYISEG'];

                        echo "<tr>";
                        echo "<td>" . $row['DATUM'] . "</td>";
                        echo "<td>" . $row['CIM'] . "</td>";
                        echo "<td>" . $row['MENNYISEG'] . "</td>";
                        echo "<td>" . $book_price . "</td>";
                        echo "</tr>";
                        $order_total += $book_price;
                    }
                    //legutolso tabla összesen ára kiirása
                    echo "<tr><td colspan='4'><strong>Összesen:</strong></td><td style='font-weight: bolder'>$order_total</td></tr>";

                    echo "</table>"; // legvegso tabla lezarasa mindig

                    oci_close($conn);
                    ?>
                </td>
            </tr>
        </table>
    </section>
</main>
</body>
</html>
