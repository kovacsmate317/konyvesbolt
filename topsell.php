<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mainpage.css">
    <title>Könyvfej</title>
</head>
<body>
<header class="color-transition">
    <h1>Könyvfej</h1>
</header>
<nav>
    <a href="index.php">Kezdőlap</a>

    <a href="kosar.php">Kosár</a>
    <a href="topsell.php" id="active">Top 5</a>
    <?php
    session_start();
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" >Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>

</nav>
<div class="container">
    <?php
    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

    if (!$conn) {
        $e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    $sql = "SELECT k.CIM AS Title,k.ID as ID ,k.KEP AS Image, SUM(rt.MENNYISEG) AS Total_Quantity
            FROM RENDELES_TETEL rt
            INNER JOIN KONYV k ON rt.KONYV_ID = k.ID
            GROUP BY k.CIM, k.KEP, k.ID
            ORDER BY SUM(rt.MENNYISEG) DESC
            FETCH FIRST 5 ROWS ONLY";
    $stmt = oci_parse($conn, $sql);
    oci_execute($stmt);


    $top_books = [];
    while ($row = oci_fetch_assoc($stmt)) {
        $top_books[] = $row;
    }

    echo '<div class="row" >';
    foreach ($top_books as $book) {
        echo '<div class="col" style="margin: auto">';
        echo '<section class="img" style="text-align: center">';
        echo '<h2>' . $book['TITLE'] . '</h2>';

        echo '<a href="egykonyv.php?id=' . $book['ID'] . '" style="margin: auto"><img src="' . $book['IMAGE'] . '" alt="' . $book['TITLE'] . '" style="height: 175px; width: 125px;"></a>';
        echo '<p>Eladott példány: ' . $book['TOTAL_QUANTITY'] . '</p>';
        echo '</section>';
        echo '</div>';
    }
    echo '</div>';

    oci_close($conn);
    ?>

</div>
</body>
</html>
