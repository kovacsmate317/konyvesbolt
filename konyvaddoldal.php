<?php
session_start();
if(!isset($_SESSION["user"])){
    header("Location: login.php");
}
if ($_SESSION["user"]["SZEREPKOR"] != "dolgozo" && $_SESSION["user"]["SZEREPKOR"] != "admin") {
    header("Location: login.php");
    exit;
}

$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = $_POST["title"];
    $publisher = $_POST["publisher"];
    $year = $_POST["year"];
    $genre = $_POST["genre"];
    $pages = $_POST["pages"];
    $binding = $_POST["binding"];
    $size = $_POST["size"];
    $price = $_POST["price"];
    $author = $_POST["author"];
    $coauthor = $_POST["coauthor"];

    $image = $_FILES["image"];
    $imageName = $image["name"];
    $imageTmpName = $image["tmp_name"];
    $imagePath = '/php/img/' . $imageName;

    move_uploaded_file($imageTmpName, $_SERVER['DOCUMENT_ROOT'] . $imagePath);

    $sql = "INSERT INTO KONYV (CIM, KIADAS, MUFAJ, OLDALSZAM, KOTES, MERET, AR, KIADO, KEP) 
            VALUES (:book_title, :book_year, :book_genre, :book_pages, :book_binding, :book_size, :book_price, :publisher, :image_path)";
    $stmt = oci_parse($conn, $sql);

    oci_bind_by_name($stmt, ':book_title', $title);
    oci_bind_by_name($stmt, ':publisher', $publisher);
    oci_bind_by_name($stmt, ':book_year', $year);
    oci_bind_by_name($stmt, ':book_genre', $genre);
    oci_bind_by_name($stmt, ':book_pages', $pages);
    oci_bind_by_name($stmt, ':book_binding', $binding);
    oci_bind_by_name($stmt, ':book_size', $size);
    oci_bind_by_name($stmt, ':book_price', $price);
    oci_bind_by_name($stmt, ':image_path', $imagePath);

    $result = oci_execute($stmt, OCI_DEFAULT);
    if (!$result) {
        oci_rollback($conn);
    } else {
        $sql = "SELECT MAX(ID) AS LAST_ID FROM KONYV";
        $stmt = oci_parse($conn, $sql);
        oci_execute($stmt);
        $row = oci_fetch_assoc($stmt);
        $lastInsertId = $row['LAST_ID'];

        $sql = "INSERT INTO SZERZO (KONYV_ID, FOSZERZO, ALSZERZO) 
                VALUES (:book_id, :author, :coauthor)";
        $stmt = oci_parse($conn, $sql);

        oci_bind_by_name($stmt, ':book_id', $lastInsertId);
        oci_bind_by_name($stmt, ':author', $author);
        oci_bind_by_name($stmt, ':coauthor', $coauthor);

        $result = oci_execute($stmt, OCI_DEFAULT); // nem commitol
        if (!$result) {
            oci_rollback($conn); // visszaallitja a commit elotti helyre az adatbazist de lehet itt most nincs ertelme?
        } else {
            oci_commit($conn);
            $sikeres = true;
        }
    }

    oci_free_statement($stmt);
    oci_close($conn);
}
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/konyvadd.css" >
    <link rel="stylesheet" href="css/mainpage.css">
    <title>Bookstore</title>
</head>
<body>

<nav>
    <a href="index.php" >Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" id="active">Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>

</nav>
<div class="container">
    <p style="text-align: center; margin: auto"><a  href="bolt.php">Bolti változtatás</a></p>
    <form action="konyvaddoldal.php" method="post" enctype="multipart/form-data" style="margin-top: 10px">
        <label for="title">Cím:</label>
        <input type="text" id="title" name="title" required>

        <label for="publisher">Kiadó:</label>
        <input type="text" id="publisher" name="publisher" required>

        <label for="author">Fő szerző:</label>
        <input type="text" id="author" name="author" required>

        <label for="coauthor">Alszerző:</label>
        <input type="text" id="coauthor" name="coauthor">

        <label for="year">Kiadás éve:</label>
        <input type="number" id="year" name="year" required>

        <label for="genre">Műfaj:</label>
        <input type="text" id="genre" name="genre" required>

        <label for="pages">Oldal szám:</label>
        <input type="number" id="pages" name="pages" required>

        <label for="binding">Kötés típus:</label>
        <input type="text" id="binding" name="binding" required>

        <label for="size">Méret:</label>
        <input type="text" id="size" name="size" required>

        <label for="price">Ár:</label>
        <input type="number" id="price" name="price" required>

        <label for="image">Borítókép (PNG or JPG):</label>
        <input type="file" id="image" name="image" accept="image/png, image/jpeg" required>

        <input type="submit" value="Hozzáadás">
        <?php if(isset($sikeres) && $sikeres && $_SERVER["REQUEST_METHOD"] === "POST"){echo "Sikeres felvitel!";} else { echo "Sikertelen felvitel!";}?>
    </form>
</div>
</body>
</html>