<?php
session_start();
if (!isset($_SESSION["user"])) {
    header("Location: login.php");
    exit;
}
if ($_SESSION["user"]["SZEREPKOR"] != "admin") {
    header("Location: index.php");
    exit;
}

$conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");
if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}

$sql = "SELECT k.MUFAJ, AVG(rt.AR) AS AVERAGE_PRICE
        FROM RENDELES_TETEL rt
        INNER JOIN KONYV k ON rt.KONYV_ID = k.ID
        GROUP BY k.MUFAJ";
$stmt = oci_parse($conn, $sql);
oci_execute($stmt);
?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/bevetel.css">
        <title>Average Price per Book Category</title>
    </head>
    <body>
    <header>
        <h1>Könyvfej</h1>
    </header>
    <nav>
        <a href="index.php">Kezdőlap</a>
        <a href="kosar.php">Kosár</a>
        <a href="topsell.php">Top 5</a>
        <?php
        if (!isset($_SESSION["user"])) {
            echo '<a href="login.php">Bejelentkezés</a>';
        }
        if (isset($_SESSION["user"])) {
            if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
                echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
            }
            echo '<a href="profile.php">Profilom</a>';
            if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
                echo '<a href="admin.php">Admin</a>';
            }
            echo '<a href="logout.php">Kijelentkezés</a>';
        }
        ?>
    </nav>

    <table>
        <tr>
            <th>Műfaj</th>
            <th>Átlag ár</th>
        </tr>
        <?php
        while ($row = oci_fetch_assoc($stmt)) {
            echo "<tr>";
            echo "<td>" . $row['MUFAJ'] . "</td>";
            echo "<td>" . $row['AVERAGE_PRICE'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>

    </body>
    </html>

<?php
oci_free_statement($stmt);
oci_close($conn);
?>