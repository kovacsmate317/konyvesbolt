<?php
session_start();

if (!isset($_SESSION['cart'])) {
    $_SESSION['cart'] = array();
}

function addToCart($bookId) {
    if (isset($_SESSION['cart'][$bookId])) {
        $_SESSION['cart'][$bookId]++;
    } else {
        $_SESSION['cart'][$bookId] = 1;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mainpage.css">
    <link rel="stylesheet" href="css/egykonyv.css">
    <title>Könyvfej</title>
</head>
<body>
<header class="color-transition">
    <h1>Könyvfej</h1>
</header>
<nav>
    <a href="index.php">Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    if (!isset($_SESSION["user"])) {
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if (isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php">Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php">Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>
</nav>
<div class="container">
    <?php

    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

    if (!$conn) {
        $e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    $book_id = $_GET['id'];

    $sql = "SELECT * FROM KONYV WHERE ID = :id";
    $stmt = oci_parse($conn, $sql);
    oci_bind_by_name($stmt, ':id', $book_id);
    oci_execute($stmt);

    $book = oci_fetch_assoc($stmt);

    echo '<div class="book-details">';
    echo '<h2>' . $book['CIM'] . '</h2>';
    echo '<img src="' . $book['KEP'] . '" alt="' . $book['CIM'] . '">';

    $sql_szerzo = "SELECT FOSZERZO, ALSZERZO FROM SZERZO WHERE KONYV_ID = :id";
    $stmt_szerzo = oci_parse($conn, $sql_szerzo);
    oci_bind_by_name($stmt_szerzo, ':id', $book_id);
    oci_execute($stmt_szerzo);

    $szerzo = oci_fetch_assoc($stmt_szerzo);
    if ($szerzo) {
        echo '<p>Szerző: ' . $szerzo['FOSZERZO'] . '</p>';
        if ($szerzo['ALSZERZO']) {
            echo '<p>Alszerző: ' . $szerzo['ALSZERZO'] . '</p>';
        }
    }
    echo '<p>Kotes: ' . $book['KOTES'] . '</p>';
    echo '<p>Oldalszam: ' . $book['OLDALSZAM'] . '</p>';
    echo '<p>Meret: ' . $book['MERET'] . '</p>';
    echo '<p>Kiado: ' . $book['KIADO'] . '</p>';

    echo '<p style="font-weight: bold; color: #007bff;">Ar: ' . $book['AR'] . ' Ft</p>';



    echo '<form method="post">';
    echo '<button type="submit" name="addToCart" value="' . $book['ID'] . '">Kosárba helyezés</button>';
    echo '</form>';
    echo '</div>';

    if (isset($_POST['addToCart'])) {
        addToCart($_POST['addToCart']);
    }

    oci_close($conn);
    ?>
</div>

</body>
</html>
