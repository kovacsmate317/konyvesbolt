<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mainpage.css">
    <title>Könyvfej</title>
</head>
<body>
<header class="color-transition">
    <h1>Könyvfej</h1>
</header>
<nav>
    <a href="index.php" id="active">Kezdőlap</a>
    <a href="kosar.php">Kosár</a>
    <a href="topsell.php">Top 5</a>
    <?php
    session_start();
    if(!isset($_SESSION["user"])){
        echo '<a href="login.php">Bejelentkezés</a>';
    }
    if(isset($_SESSION["user"])) {
        if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="konyvaddoldal.php" >Könyv felvitel</a>';
        }
        echo '<a href="profile.php">Profilom</a>';
        if ($_SESSION["user"]["SZEREPKOR"] === "admin") {
            echo '<a href="admin.php" >Admin</a>';
        }
        echo '<a href="logout.php">Kijelentkezés</a>';
    }
    ?>

</nav>
<div class="container">
    <?php
    $conn = oci_pconnect("bambi", "Janika23", "//localhost:1521/XE", "UTF8");

    if (!$conn) {
        $e = oci_error();
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    }

    $sql = "SELECT * FROM KONYV";
    $stmt = oci_parse($conn, $sql);
    oci_execute($stmt);

    $books = [];
    while ($row = oci_fetch_assoc($stmt)) {
        $books[] = $row;
    }

    function displayBookContainers($books) {
        echo '<div class="row">';
        foreach ($books as $book) {
            echo '<div class="col">';
            echo '<section class="img">';
            echo '<h2>' . $book['CIM'] . '</h2>';
            echo '<a href="egykonyv.php?id=' . $book['ID'] . '" style="margin: auto"><img src="' . $book['KEP'] . '" alt="' . $book['CIM'] . '" style="height: 175px; width: 125px;"></a>';
            if(isset($_SESSION["user"])) {
                if ($_SESSION["user"]["SZEREPKOR"] === "dolgozo" || $_SESSION["user"]["SZEREPKOR"] === "admin") {
                    echo '<p><a href="delete_book.php?id=' . $book['ID'] . '">Törlés</a> | <a href="update.php?id=' . $book['ID'] . '">Módosítás</a></p>';
                }
            }
            echo '</section>';
            echo '</div>';
        }
        echo '</div>';
    }

    displayBookContainers($books);

    oci_close($conn);
    ?>

</div>
</body>
</html>
